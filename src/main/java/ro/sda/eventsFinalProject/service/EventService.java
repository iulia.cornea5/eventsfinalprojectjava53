package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.repository.EventRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class EventService {

    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event save(Event event) {
        if(event == null) {
            throw new IllegalArgumentException("Event must not be null on save");
        }
        if (event.getName() == null) {
            throw new IllegalArgumentException("All events must have a name");
        }
        if (event.getStartDate() == null || event.getEndDate() == null || event.getStartDate().isAfter(event.getEndDate())) {
            throw new IllegalArgumentException("Start date must be the same or before as the end date.");
        }
        Event savedEvent = eventRepository.save(event);
        return savedEvent;
    }

    public Event read(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("Event id must not be null.");
        }
        try {
            Event event = eventRepository.findById(id).get();
            return event;
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("There is no event with id " + id);
        }
    }

    public List<Event> readAll() {
        List<Event> events = eventRepository.findAll();
        return events;
    }

    public Event update(Event updatedEvent) {
        if(updatedEvent == null) {
            throw new IllegalArgumentException("Event must not be null on save");
        }
        // verificăm că există un event cu id-ul dat în baza de date
        Event oldEvent = read(updatedEvent.getId());
        // salvăm (cu toate verificările de rigoare) evenimentul în baza de date
        return save(updatedEvent);
    }

    public void delete(Integer eventId) {
        Event eventToDelete = read(eventId);
        eventRepository.delete(eventToDelete);
    }
}
