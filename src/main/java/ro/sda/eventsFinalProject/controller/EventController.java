package ro.sda.eventsFinalProject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.service.EventService;

import java.util.List;

// http://localhost:8080/...
@RestController
public class EventController {

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    // POST http://localhost:8080/events
    @PostMapping("/events")
    public ResponseEntity create(@RequestBody Event event) {
        if (event.getId() != null) {
            return new ResponseEntity("New events are not allowed to have a predefined id", HttpStatus.BAD_REQUEST);
        }
        try {
            Event saved = eventService.save(event);
            return new ResponseEntity(saved, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/events/{id}")
    public ResponseEntity read(@PathVariable(name = "id") Integer eventId) {
        try {
            Event event = eventService.read(eventId);
            return new ResponseEntity(event, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/events")
    public ResponseEntity readAll() {
        List<Event> events = eventService.readAll();
        return new ResponseEntity(events, HttpStatus.OK);
    }

    @PutMapping("/events/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Integer pathId, @RequestBody Event eventToUpdate) {
        if (!pathId.equals(eventToUpdate.getId())) {
            return new ResponseEntity("Inconsistent ids", HttpStatus.BAD_REQUEST);
        }
        try {
            Event updated = eventService.update(eventToUpdate);
            return new ResponseEntity(updated, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/events/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        try {
            eventService.delete(id);
            return new ResponseEntity("Event deleted successfully", HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
